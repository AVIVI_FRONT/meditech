'use strict';
if(!window.console) window.console = {};
if(!window.console.memory) window.console.memory = function() {};
if(!window.console.debug) window.console.debug = function() {};
if(!window.console.error) window.console.error = function() {};
if(!window.console.info) window.console.info = function() {};
if(!window.console.log) window.console.log = function() {};

// sticky footer
//-----------------------------------------------------------------------------
if(!Modernizr.flexbox) {
  (function() {
    var
      $pageWrapper = $('#page-wrapper'),
      $pageBody = $('#page-body'),
      noFlexboxStickyFooter = function() {
        $pageBody.height('auto');
        if($pageBody.height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
          $pageBody.height($(window).height() - $('#header').outerHeight() - $('#footer').outerHeight());
        } else {
          $pageWrapper.height('auto');
        }
      };
    $(window).on('load resize', noFlexboxStickyFooter);
  })();
}
if(ieDetector.ieVersion == 10 || ieDetector.ieVersion == 11) {
  (function(){
    var
      $pageWrapper = $('#page-wrapper'),
      $pageBody = $('#page-body'),
      ieFlexboxFix = function() {
        if($pageBody.addClass('flex-none').height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
          $pageWrapper.height($(window).height());
          $pageBody.removeClass('flex-none');
        } else {
          $pageWrapper.height('auto');
        }
      };
    ieFlexboxFix();
    $(window).on('load resize', ieFlexboxFix);
  })();
}

$(function() {

// placeholder
//-----------------------------------------------------------------------------

    $('input[placeholder], textarea[placeholder]').placeholder();

    function banner() {
        var slider = $('#banner-slider'),
            arrows = $('.js-banner-arrows');
        slider.slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            speed: 300,
            fade: true,
            cssEase: 'linear',
            arrows: false,
            dots: true,
        });
        arrows.on('click', function(e) {
            e.preventDefault();
            slider.slick($(this).attr('data-slider'));
        });
    }

    function clientsSlider() {
        var slider = $('#clients-slider');
        slider.slick({
            infinite: true,
            slidesToShow: 8,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            autoplay: true,
            autoplaySpeed: 2000,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 1,
                    }
                },
            ],
        });
    }

    function mobileMenu() {
        var btn = $('#mobile-menu'),
            menu = $('#menu'),
            submenu = $('.menu__item.dropdown'),
            body = $('body');

        menu.removeClass('active');
        btn.removeClass('active');
        submenu.removeClass('open');
        body.removeClass('hidden');

        if ($(window).width()<769) {
            btn.on('click', function () {
                menu.toggleClass('active');
                $(this).toggleClass('active');
                body.toggleClass('hidden');
            });
            submenu.on('click', function () {
                $(this).toggleClass('open');
            });
        }
    }

    function portfolioPopup() {
        var portfolioItem = $('.portfolio__wrapper [data-fancybox=""]');
        portfolioItem.each(function () {
            var thisID = $(this).attr('data-src');
            $(this).fancybox({
                touch: false,
                arrows: false,
                afterShow : function() {
                    var slider = $(thisID).find('.js-portfolio-slider');
                    var arrows = slider.parents('.portfolio__popup').find('.js-portfolio-arrows');
                    slider.on('init', function(event, slick){
                        $(this).parents('.portfolio__popup').removeClass('hidden');
                        var sliderThat = slider;
                        arrows.on('click', function(e) {
                            e.preventDefault();
                            sliderThat.slick($(this).attr('data-slider'));
                        });
                    });
                    slider.slick({
                        infinite: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false,
                        dots: false,
                    });
                    function curentSlide(){
                        var curSlide = slider.slick('slickCurrentSlide') + 1;
                        slider.parents('.portfolio__popup').find('.outof').text(curSlide);
                    }
                    function totalSlides(){
                        var total = slider.find($('.slick-slide:not(.slick-cloned)')).length;
                        slider.parents('.portfolio__popup').find('.total').text(total);
                    }
                    curentSlide();
                    totalSlides();
                    slider.on('afterChange', function(event, slick, currentSlide){
                        curentSlide();
                    });
                },
            });
        });
    }

    banner();
    clientsSlider();
    mobileMenu();
    portfolioPopup();

});
