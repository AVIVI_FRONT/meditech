function initDetailSlider() {
    var slider = $('.detail__slider'),
        arrows = $('.js-detail-arrows');
    slider.slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        speed: 300,
        cssEase: 'linear',
        arrows: false,
        dots: true
    });
    arrows.on('click', function(e) {
        e.preventDefault();
        slider.slick($(this).attr('data-slider'));
    });
}

function setDetailArrowsPosition() {
    var arrows = $('.js-detail-arrows'),
        imgHeight = $('.detail__img').innerHeight();
    arrows.css('top', imgHeight / 2 - arrows.innerHeight() / 2 - 5);
}

function fileSize(size) {
    var i = Math.floor( Math.log(size) / Math.log(1024) );
    return ( size / Math.pow(1024, i) ).toFixed(2) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
}

var inputFile = document.getElementById('input-file');
var formFile = document.getElementById('form-file');

function inputHandler() {
    document.getElementById('output').innerHTML = '';
    var element = document.getElementById(this.id),
        result = element.files;
    for (var x = 0; x < result.length; x++) {
        var file = result[x],
            li  = document.createElement("li");
        li.className = "form__uploaded-item";
        li.innerHTML =
            "<span class=\"form__clips ico \">" +
                "<svg class=\"icon icon-ico_left \">" +
                    "<use xlink:href=\"img/sprite.svg#clips\"></use>" +
                "</svg>" +
            "</span>" +
            "<span>" +
            "<span>" + file.name + "</span>" + "<span class=\"form__uploaded-size\">" + "(" + fileSize(file.size) + ")" + "</span>" + "</span>" + "<span class=\"form__clips-del\"></span>";
        document.getElementById('output').appendChild(li);
    }
}
function formHandler() {
    document.getElementById('form-output').innerHTML = '';
    var element = document.getElementById(this.id),
        result = element.files;
    for (var x = 0; x < result.length; x++) {
        var file = result[x],
            li  = document.createElement("li");
        li.className = "form__uploaded-item";
        li.innerHTML =
            "<span class=\"form__clips ico \">" +
            "<svg class=\"icon icon-ico_left \">" +
            "<use xlink:href=\"img/sprite.svg#clips\"></use>" +
            "</svg>" +
            "</span>" +
            "<span>" +
            "<span>" + file.name + "</span>" + "<span class=\"form__uploaded-size\">" + "(" + fileSize(file.size) + ")" + "</span>" + "</span>" + "<span class=\"form__clips-del\"></span>";
        document.getElementById('form-output').appendChild(li);
    }
}

function priceTabs() {
    var hash = window.location.hash;
    if (hash) {
        $('.price__tab').removeClass('active');
        $('.price__detail').removeClass('active');
        $(hash).addClass('active');
        $('.price__tab').each(function () {
            var href = $(this).attr('href');
            if(href == hash) {
                $(this).addClass('active');
            }
        });
    }

    $('.price__tab').on('click', function (e) {
        e.preventDefault();
        $('.price__tab').removeClass('active');
        $(this).addClass('active');
        var tab = $(this).attr('href');
        $('.price__detail').removeClass('active');
        $(tab).addClass('active');
        $('.price__tabs').removeClass('opened');

    });
}

function openPriceSelect() {
    $('.price__tab-open').on('click', function () {
        $('.price__tabs').toggleClass('opened');
    })
}

$(function() {
    initDetailSlider();
    setDetailArrowsPosition();

    $(window).resize(function () {
        setDetailArrowsPosition();
    });

    $('.form').validate();
    $('#specials-request').validate();
    $('#services-request').validate();
    $('#order-request').validate();
    jQuery.extend(jQuery.validator.messages, {
        required: "Поле обязательно для заполнения",
        remote: "Please fix this field.",
        email: "Неправильный формат",
        url: "Please enter a valid URL.",
        date: "Неправильный формат",
        dateISO: "Please enter a valid date (ISO).",
        number: "Неправильный формат",
        digits: "Please enter only digits.",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Please enter the same value again.",
        accept: "Please enter a value with a valid extension.",
        maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
        minlength: jQuery.validator.format("Please enter at least {0} characters."),
        rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
        range: jQuery.validator.format("Please enter a value between {0} and {1}."),
        max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
        min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
    });

    $(document).on('click', '.form__clips-del', function () {
        $(this).parents('.form__uploaded-item').remove();
    });

    if(inputFile) {
        inputFile.addEventListener('change', inputHandler, false);
    }
    if(formFile) {
        formFile.addEventListener('change', formHandler, false);
    }

    priceTabs();
    openPriceSelect();

    $(".scroll-wrapper").mCustomScrollbar({
        axis:"x"
    });
});